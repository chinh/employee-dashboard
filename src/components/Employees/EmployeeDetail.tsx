/* eslint-disable react/no-unknown-property */
import React from "react";

// type
import {EmployeeItemType} from "./index"


const EmployeesDetail = ({employee}:{employee:EmployeeItemType}) => {

  return (
    <div className="employees-detail">
      <div className="detail-image">
        <img src={`./images/employees/${employee.image.replace(" ", "")}`}></img>
      </div>
      <div className="detail-info">
        <p className="detail-name">{employee.name}</p>
        <div className="detail-popularity"><p className="detail-label">Popularity</p><input type="range" id="popularity" name="vol" min="0" max="10" value={employee.popularity} readOnly></input></div>
        <div className="detail-biography"><p className="detail-label">Biography</p>{employee.biography}</div>
      </div>
      
    </div>
    
  );
};

export default EmployeesDetail;