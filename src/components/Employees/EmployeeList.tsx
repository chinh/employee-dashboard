import React from "react";

// redux


// data
import employeesData from "../../data/EmployeeData.json";

// type
import {EmployeeItemType} from "./index"

interface EmployeesListType {
  currentEmployee: EmployeeItemType;
  setCurrentEmployee: (employee:EmployeeItemType) => void;
}

const EmployeesList = ({currentEmployee,setCurrentEmployee }:EmployeesListType) => {
  console.log("employeeData",{employeesData})

  const setActive = (employee:EmployeeItemType) => {
    typeof setCurrentEmployee === "function" && setCurrentEmployee(employee)
  }

  return (
    <div className="employees-list">
      <ul className="list">{employeesData.employees.map(employee => {
        const classSize = `size-${employee.popularity}`;
        const classHascolleagues = employee.colleagues.length ? "has-colleagues": "";
        const classActive = employee.name === currentEmployee.name ? "active" : "";
        return <li key={employee.name.replace(" ", "")} className={`item-list ${classSize} ${classHascolleagues} ${classActive}`} onClick={() => setActive(employee)}>{employee.name}</li>
      }
        
      )}
      </ul>
    </div>
    
  );
};

export default EmployeesList;