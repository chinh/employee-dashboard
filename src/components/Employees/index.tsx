import React, { useState } from "react";
import EmployeeList from "./EmployeeList"
import EmployeeDetail from "./EmployeeDetail"
// redux


// data
import employeesData from "../../data/EmployeeData.json"

export interface EmployeeItemType {
  name: string,
  popularity: number,
  biography: string,
  image: string,
  colleagues: string[]
}

const Employees = () => {
  
  const [currentEmployee, setCurrentEmployee] = useState(employeesData.employees[0])

  console.log("currentEmployee",{currentEmployee})

  return (
    <div className="employees">
      <EmployeeList currentEmployee={currentEmployee} setCurrentEmployee={setCurrentEmployee}/>
      <EmployeeDetail employee={currentEmployee} />
    </div>
    
  );
};

export default Employees;