import React from "react";
import Employees from "components/Employees"

const App = () => {


  return (
      <div className="container">
        <div className="header">
          <div className="logo-content">
            <img src="./images/the-godfather.svg"></img>
          </div>
        </div>
          <Employees />
      </div>
  );
};

export default App;
